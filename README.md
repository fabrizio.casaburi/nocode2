# Team: NoCode2

> **Team Members** Alessandro Parisi, Bruno Del Prete, Fabrizio Casaburi, Gennaro Cotarella  
> **Challenge** Nano Challenge 2  
> **Track** Front-End [Katherine Johnson]  
> **Topic** Gestures & Sensors  
> **Learning Objectives** CD34, CD35, CD58  

## Project Description

For this Nano Challenge, we decided to let everyone in the group to focus on what mattered him the most.
In order to do this, we created a single Xcode project but we divided it in as many tab as we are: each of us developed its own tab experimenting with the kits and technologies of his own choosing.

### Alessandro's Tab

### Bruno's Tab

In my tab, I focused on gestures, implementing the following UIGestureRecognizer subclasses via storyboard:

- UIPanGestureRecognizer
- UIPinchGestureRecognizer
- UIRotationGestureRecognizer
- UITapGestureRecognizer
- UISwipeGestureRecognizer
- UILongPressGestureRecognizer

In addition to that, I implemented vibration when long press is detected and deceleration for the imageview when the pangesture is released.

### Fabrizio's Tab

In my tab, I focused on gestures, implementing the following UIGestureRecognizer subclasses via code:

- UIPanGestureRecognizer
- UIPinchGestureRecognizer
- UIRotationGestureRecognizer
- UITapGestureRecognizer

In addition to that, I used AVFoundation to play a sound when the user taps on the monkey or on the banana.

In order to developed my tab, I relied primarily on the following resource: [https://www.raywenderlich.com/9225-gesture-recognizers-in-ios]()

### Gennaro's Tab

