//
//  AlessandroViewController.swift
//  NoCode2
//
//  Created by Alessandro Parisi on 06/03/2019.
//  Copyright © 2019 NoCode2. All rights reserved.
//

import UIKit
import CoreMotion
class AlessandroViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var test: UIImageView!
    @IBOutlet weak var moduloLabel: UILabel!
    
    @IBOutlet weak var accICON: UIButton!
    @IBOutlet weak var gyroICON: UIButton!
    @IBOutlet weak var magnICON: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var xField: UILabel!
    @IBOutlet weak var yField: UILabel!
    @IBOutlet weak var zField: UILabel!
    @IBOutlet weak var xBest: UILabel!
    @IBOutlet weak var yBest: UILabel!
    @IBOutlet weak var zBest: UILabel!
    @IBOutlet weak var xBestACC: UILabel!
    @IBOutlet weak var yBestACC: UILabel!
    @IBOutlet weak var zBestACC: UILabel!
    @IBOutlet weak var xUnit: UILabel!
    @IBOutlet weak var yUnit: UILabel!
    @IBOutlet weak var zUnit: UILabel!
    @IBOutlet weak var xBestMAG: UILabel!
    @IBOutlet weak var yBestMAG: UILabel!
    @IBOutlet weak var zBestMAG: UILabel!
    @IBOutlet weak var xBestROT: UILabel!
    @IBOutlet weak var yBestROT: UILabel!
    @IBOutlet weak var zBestRot: UILabel!
    
    
    var tx: Double = -999
    var ty: Double = -999
    var tz: Double = -999
    var check: Bool = true
    var magnmotion = CMMotionManager()
    var gyromotion = CMMotionManager()
    var accmotion = CMMotionManager()
    var devicemotion = CMMotionManager()
    var timer: Timer?
    
    var choice: Int = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.xBestROT.isHidden = true
        self.yBestROT.isHidden = true
        self.zBestRot.isHidden = true
        self.xBestACC.isHidden = true
        self.yBestACC.isHidden = true
        self.zBestACC.isHidden = true
        self.xBest.isHidden = true
        self.yBest.isHidden = true
        self.zBest.isHidden = true
        self.xBestMAG.isHidden = true
        self.yBestMAG.isHidden = true
        self.zBestMAG.isHidden = true
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinch(sender:)))
        pinch.delegate = self
        self.test.addGestureRecognizer(pinch)
        
        
        let panI = UIPanGestureRecognizer(target: self, action: #selector(dragFunc))
        panI.delegate = self
        
        
        let rotI = UIRotationGestureRecognizer(target: self, action: #selector(rotationGest))
        rotI.delegate = self
        
        
        test.addGestureRecognizer(rotI)
        test.addGestureRecognizer(panI)
        
        
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func motionButton(_ sender: Any) {
        xUnit.text = "°"
        yUnit.text = "°"
        zUnit.text = "°"
        
        tx = -999
        ty = -999
        tz = -999
        
        self.xBestROT.isHidden = false
        self.yBestROT.isHidden = false
        self.zBestRot.isHidden = false
        
        self.xBestMAG.isHidden = true
        self.yBestMAG.isHidden = true
        self.zBestMAG.isHidden = true
        
        self.xBest.isHidden = true
        self.yBest.isHidden = true
        self.zBest.isHidden = true
        
        self.xBestACC.isHidden = true
        self.yBestACC.isHidden = true
        self.zBestACC.isHidden = true
        
        self.magnmotion.stopMagnetometerUpdates()
        self.accmotion.stopAccelerometerUpdates()
        self.gyromotion.stopGyroUpdates()
        titleLabel.text = "MotionTest"
        motionStart()
        
    }
    @IBAction func magnButton(_ sender: Any) {
        xUnit.text = "uT"
        yUnit.text = "uT"
        zUnit.text = "uT"
        tx = -999
        ty = -999
        tz = -999
        self.xBestMAG.isHidden = false
        self.yBestMAG.isHidden = false
        self.zBestMAG.isHidden = false
        
        self.xBestROT.isHidden = true
        self.yBestROT.isHidden = true
        self.zBestRot.isHidden = true
        
        self.xBest.isHidden = true
        self.yBest.isHidden = true
        self.zBest.isHidden = true
        
        self.xBestACC.isHidden = true
        self.yBestACC.isHidden = true
        self.zBestACC.isHidden = true
        
        self.devicemotion.stopDeviceMotionUpdates()
        self.accmotion.stopAccelerometerUpdates()
        self.gyromotion.stopGyroUpdates()
        titleLabel.text = "MagnTest"
        magnStart()
    }
    @IBAction func accButton(_ sender: Any) {
        xUnit.text = "m/(s^2)"
        yUnit.text = "m/(s^2)"
        zUnit.text = "m/(s^2)"
        tx = -999
        ty = -999
        tz = -999
        self.xBestACC.isHidden = false
        self.yBestACC.isHidden = false
        self.zBestACC.isHidden = false
        
        self.xBestROT.isHidden = true
        self.yBestROT.isHidden = true
        self.zBestRot.isHidden = true
        
        self.xBestMAG.isHidden = true
        self.yBestMAG.isHidden = true
        self.zBestMAG.isHidden = true
        
        self.xBest.isHidden = true
        self.yBest.isHidden = true
        self.zBest.isHidden = true
        
        self.devicemotion.stopDeviceMotionUpdates()
        self.gyromotion.stopGyroUpdates()
        self.magnmotion.stopMagnetometerUpdates()
        titleLabel.text = "AccTest"
        accStart()
    }
    @IBAction func gyroButton(_ sender: Any) {
        xUnit.text = "°/s"
        yUnit.text = "°/s"
        zUnit.text = "°/s"
        tx = -999
        ty = -999
        tz = -999
        self.xBestACC.isHidden = true
        self.yBestACC.isHidden = true
        self.zBestACC.isHidden = true
        
        self.xBestROT.isHidden = true
        self.yBestROT.isHidden = true
        self.zBestRot.isHidden = true
        
        self.xBestMAG.isHidden = true
        self.yBestMAG.isHidden = true
        self.zBestMAG.isHidden = true
        
        self.xBest.isHidden = false
        self.yBest.isHidden = false
        self.zBest.isHidden = false
        
        self.devicemotion.stopDeviceMotionUpdates()
        self.accmotion.stopAccelerometerUpdates()
        self.magnmotion.stopMagnetometerUpdates()
        titleLabel.text = "GyroTest"
        gyroStart()
    }
    
    func gyroStart(){
        choice = 1
        self.gyromotion.gyroUpdateInterval = 0
        self.gyromotion.startGyroUpdates()
        if choice == 1{
            self.timer = Timer(fire: Date(), interval: 0, repeats: true, block: { (timer) in
                if let data = self.gyromotion.gyroData{
                    let x = data.rotationRate.x * 180/3.14
                    let y = data.rotationRate.y * 180/3.14
                    let z = data.rotationRate.z * 180/3.14
                    if self.choice == 1{
                        self.limitLabel(l: self.xField, r: x)
                        self.limitLabel(l: self.yField, r: y)
                        self.limitLabel(l: self.zField, r: z)
                        
                        self.bestLabel(tn: &self.tx, n: x, l: self.xBest)
                        self.bestLabel(tn: &self.ty, n: y, l: self.yBest)
                        self.bestLabel(tn: &self.tz, n: z, l: self.zBest)
                        self.moduloVettore(x: x, y: y, z: z)
                    }
                }
            })
            RunLoop.current.add(self.timer!, forMode: .default)
        }
    }
    func accStart(){
        choice = 2
        self.accmotion.accelerometerUpdateInterval = 0
        self.accmotion.startAccelerometerUpdates()
        if choice == 2{
            self.timer = Timer(fire: Date(), interval: 0, repeats: true, block: {(timer) in
                if let data = self.accmotion.accelerometerData{
                    let x = data.acceleration.x
                    let y = data.acceleration.y
                    let z = data.acceleration.z
                    if self.choice == 2{
                        self.limitLabel(l: self.xField, r: x)
                        self.limitLabel(l: self.yField, r: y)
                        self.limitLabel(l: self.zField, r: z)}
                    
                    self.bestLabel(tn: &self.tx, n: x, l: self.xBestACC)
                    self.bestLabel(tn: &self.ty, n: y, l: self.yBestACC)
                    self.bestLabel(tn: &self.tz, n: z, l: self.zBestACC)
                    self.moduloVettore(x: x, y: y, z: z )
                }
            })
            RunLoop.current.add(self.timer!, forMode: .default)
        }
    }
    
    func magnStart(){
        choice = 3
        self.magnmotion.magnetometerUpdateInterval = 0
        self.magnmotion.startMagnetometerUpdates()
        if choice == 3{
            self.timer = Timer(fire: Date(), interval: 0, repeats: true, block: {(timer) in
                if let data = self.magnmotion.magnetometerData{
                    let x = data.magneticField.x
                    let y = data.magneticField.y
                    let z = data.magneticField.z
                    if self.choice == 3{
                        self.limitLabel(l: self.xField, r: x)
                        self.limitLabel(l: self.yField, r: y)
                        self.limitLabel(l: self.zField, r: z)
                        
                        self.bestLabel(tn: &self.tx, n: x, l: self.xBestMAG)
                        self.bestLabel(tn: &self.ty, n: y, l: self.yBestMAG)
                        self.bestLabel(tn: &self.tz, n: z, l: self.zBestMAG)
                        self.moduloVettore(x: x, y: y, z: z)
                        
                    }
                }
            })
            RunLoop.current.add(self.timer!, forMode: .default)
        }
        
    }
    
    func motionStart(){
        choice = 4
        self.devicemotion.deviceMotionUpdateInterval = 0
        
        if choice == 4{
            self.devicemotion.startDeviceMotionUpdates(to: OperationQueue.current!){
                (data,error) in print(data as Any)
                if let truedata = data{
                    self.reloadInputViews()
                    let mpitch = truedata.attitude.pitch * 180/3.14
                    let mroll = truedata.attitude.roll   * 180/3.14
                    let myaw = truedata.attitude.yaw     * 180/3.14
                    
                    
                    if self.choice == 4{
                        let yaw = myaw * 3.14/180
                        self.test.transform = CGAffineTransform(rotationAngle: CGFloat(yaw))
                        self.limitLabel(l: self.xField, r: mpitch)
                        self.limitLabel(l: self.yField, r: mroll)
                        self.limitLabel(l: self.zField, r: myaw)
                        
                        self.limitLabel(l: self.xBestROT, r: mpitch)
                        self.limitLabel(l: self.yBestROT, r: mroll)
                        self.limitLabel(l: self.zBestRot, r: myaw)
                        
                        //                        self.bestLabel(tn: &self.tx, n: mpitch, l: self.xBestROT)
                        //                        self.bestLabel(tn: &self.ty, n: mroll, l: self.yBestROT)
                        //                        self.bestLabel(tn: &self.tz, n: myaw, l: self.zBestRot)
                        self.moduloVettore(x: mpitch, y: mroll, z: myaw)
                        
                        
                    }
                    
                }
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func bestLabel(tn: inout Double, n: Double , l:UILabel){
        if tn >= n {
            limitLabel(l: l, r: tn)
        }
        else if tn < n{
            tn = n
        }
    }
    
    func limitLabel(l: UILabel, r: Double){
        var textOnLabel = "\(r)"
        if(textOnLabel.count > 5){
            
            textOnLabel = String(textOnLabel[textOnLabel.startIndex..<textOnLabel.index(textOnLabel.startIndex, offsetBy: 5)])
        }
        
        l.text = textOnLabel
    }
    
    @objc func dragFunc(recognizer: UIPanGestureRecognizer){
        let translation = recognizer.translation(in: self.view)
        if let view = recognizer.view{
            view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @objc func pinch(sender: UIPinchGestureRecognizer){
        test.transform = test.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
        switch sender.state{
        case .began: print("Start")
        case .ended: print("End")
        case .changed: print("change")
        default:
            break
        }
        
        
    }
    @objc func rotationGest(sender: UIRotationGestureRecognizer){
        test.transform = test.transform.rotated(by: sender.rotation)
        sender.rotation = 0
    }
    func moduloVettore(x: Double, y:Double, z:Double){
        let modulo = ((x * x) + (y * y) + (z * z)).squareRoot()
        limitLabel(l: moduloLabel, r: modulo)
    }
}
extension AlessandroViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith _: UIGestureRecognizer) -> Bool {
        return true
    }
}
