//
//  FabrizioViewController.swift
//  NoCode2
//
//  Created by Fabrizio Casaburi on 06/03/2019.
//  Copyright © 2019 NoCode2. All rights reserved.
//

import UIKit
import AVFoundation

class FabrizioViewController: UIViewController {
    
    let chompPlayer = AVAudioPlayer(fileName: "chomp")
    
    @IBOutlet weak var monkey: UIImageView! {
        didSet {
            let monkeyPanRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
            monkeyPanRecognizer.delegate = self
            monkey.addGestureRecognizer(monkeyPanRecognizer)
            
            let monkeyPinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
            monkeyPinchRecognizer.delegate = self
            monkey.addGestureRecognizer(monkeyPinchRecognizer)
            
            let monkeyRotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(handleRotation))
            monkeyRotationRecognizer.delegate = self
            monkey.addGestureRecognizer(monkeyRotationRecognizer)
        }
    }

    @IBOutlet weak var banana: UIImageView! {
        didSet {
            let bananaPanRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
            bananaPanRecognizer.delegate = self
            banana.addGestureRecognizer(bananaPanRecognizer)
            
            let bananaPinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
            bananaPinchRecognizer.delegate = self
            banana.addGestureRecognizer(bananaPinchRecognizer)
            
            let bananaRotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(handleRotation))
            bananaRotationRecognizer.delegate = self
            banana.addGestureRecognizer(bananaRotationRecognizer)
        }
    }

    
    var monkeyOriginalPosition: CGPoint!
    var bananaOriginalPosition: CGPoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactiveSubviews.map {
            $0.gestureRecognizers!.first { $0 is UIPanGestureRecognizer }!
            }
            .forEach { panRecognizer in
                panRecognizer.view!.gestureRecognizers!
                    .first { $0 is UITapGestureRecognizer }!
                    .require(toFail: panRecognizer)
        }
        
        monkeyOriginalPosition = monkey.center
        bananaOriginalPosition = banana.center
        
    }
    
    @IBOutlet var jungle: UIImageView! {
        didSet {
            let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
            doubleTapRecognizer.delegate = self
            doubleTapRecognizer.numberOfTapsRequired = 2
            doubleTapRecognizer.numberOfTouchesRequired = 2
            jungle.addGestureRecognizer(doubleTapRecognizer)
        }
    }
    
    @IBOutlet var interactiveSubviews: [UIImageView]! {
        didSet {
            for subview in interactiveSubviews {
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                tapRecognizer.delegate = self
                subview.addGestureRecognizer(tapRecognizer)
            }
        }
    }
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer) {
        guard let recognizerView = recognizer.view else {
            return
        }
        
        let translation = recognizer.translation(in: view)
        recognizerView.center.x += translation.x
        recognizerView.center.y += translation.y
        recognizer.setTranslation(.zero, in: view)
        
        guard recognizer.state == .ended else {
            return
        }
        
        let velocity = recognizer.velocity(in: view)
        let vectorToFinalPoint = CGPoint(x: velocity.x / 15, y: velocity.y / 15)
        let bounds = view.bounds.inset(by: view.safeAreaInsets)
        var finalPoint = recognizerView.center
        finalPoint.x += vectorToFinalPoint.x
        finalPoint.y += vectorToFinalPoint.y
        finalPoint.x = min(max(finalPoint.x, bounds.minX), bounds.maxX)
        finalPoint.y = min(max(finalPoint.y, bounds.minY), bounds.maxY)
        let vectorToFinalPointLength = ( vectorToFinalPoint.x * vectorToFinalPoint.x + vectorToFinalPoint.y * vectorToFinalPoint.y
            ).squareRoot()
        
        UIView.animate(withDuration: TimeInterval(vectorToFinalPointLength / 1800), delay: 0, options: .curveEaseOut, animations: {
            recognizerView.center = finalPoint
        })
    }
    
    @objc func handlePinch(_ recognizer: UIPinchGestureRecognizer) {
        guard let recognizerView = recognizer.view else {
            return
        }
        
        recognizerView.transform = recognizerView.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
        recognizer.scale = 1
    }
    
    
    @objc func handleRotation(_ recognizer: UIRotationGestureRecognizer) {
        guard let recognizerView = recognizer.view else {
            return
        }
        
        recognizerView.transform = recognizerView.transform.rotated(by: recognizer.rotation)
        recognizer.rotation = 0
    }
    
    @objc func handleTap (_: UITapGestureRecognizer) {
        chompPlayer.play()
    }
    
    @objc func handleDoubleTap (_: UITapGestureRecognizer) {
        print("DoubleTap Detected")
        
        UIView.animate(withDuration: TimeInterval(0.5), delay: 0, options: .curveEaseOut, animations: {
            self.monkey.center = self.monkeyOriginalPosition
            self.banana.center = self.bananaOriginalPosition
        })
        
        
    }
    
}

extension FabrizioViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}


 extension AVAudioPlayer {
    convenience init(fileName: String) {
        let url = Bundle.main.url(forResource: fileName, withExtension: "caf")!
        try! self.init(contentsOf: url)
        prepareToPlay()
    }
}
