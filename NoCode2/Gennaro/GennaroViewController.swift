//
//  GennaroViewController.swift
//  NoCode2
//
//  Created by Gennaro Cotarella on 07/03/2019.
//  Copyright © 2019 NoCode2. All rights reserved.
//

import UIKit
import CoreMotion

class GennaroViewController: UIViewController {

    @IBOutlet weak var immagine: UIImageView!
    var pinch : UIPinchGestureRecognizer!
    var motion = CMMotionManager()
    var timer : Timer?
    @IBOutlet weak var ramo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Creo pinch gesture da codice
        pinch = UIPinchGestureRecognizer()
        pinch.scale = 1
        pinch.isEnabled = true
        pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchCodice(_:)))
        self.view.addGestureRecognizer(pinch)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(dragFunc))
        pan.delegate = self
        self.ramo.addGestureRecognizer(pan)
        pinch.delegate = self
        let rot = UIRotationGestureRecognizer(target: self, action: #selector(rotationGest(sender:)))
        rot.delegate = self
        self.ramo.addGestureRecognizer(rot)
        motionStart()
        
        
        
       
        
        
    }
    
    //Metodo Pinch da codice
    
   @IBAction func pinchCodice(_ sender: UIPinchGestureRecognizer){
        
        ramo.transform = ramo.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
        
        switch sender.state{
        case .began:
            print("Began")
        
        case .ended:
            print("Ended")
            
        case .possible:
            print("Possible")
            
        default:
            break
        }
 
        //Ritorno alla forma iniziale alla fine della chiamata della action Pinch
        
        if sender.state == .ended{
            self.immagine.transform = CGAffineTransform.identity
        }
    }
    
    var averageRotation: [Double] = []
    var maxAverage = 1
    
    func motionStart(){
        motion.deviceMotionUpdateInterval = 0
        motion.startDeviceMotionUpdates(to: OperationQueue.current!){ (data, error) in
            if let trueData = data {
                self.reloadInputViews()
                var y = trueData.attitude.yaw * 5
                
                addToAverageArray(roll: y)
                self.rotationImmagine(n: getAverageRotation())
                
            }
    }
        
        func addToAverageArray(roll: Double){
            if(averageRotation.count >= maxAverage){
                averageRotation.remove(at: 0)
            }
            averageRotation.append(roll)
        }
        
        func getAverageRotation() -> Double{
            return averageRotation.reduce(into: 0, { (res, val) in
                res = res + val
            }) / Double(averageRotation.count)
        }
}
    func rotationImmagine(n: Double){
        immagine.transform = CGAffineTransform(rotationAngle: CGFloat(n))
        print(n)
    }
    
    @objc func dragFunc(recognizer: UIPanGestureRecognizer){
        let translation = recognizer.translation(in: self.view)
        if let view = recognizer.view{
            view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    @objc func rotationGest(sender: UIRotationGestureRecognizer){
        ramo.transform = ramo.transform.rotated(by: sender.rotation)
        sender.rotation = 0
    }
    

}
extension GennaroViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith _: UIGestureRecognizer) -> Bool {
        return true
    }
}
 

