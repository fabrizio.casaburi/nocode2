//
//  BrunoViewController.swift
//  NoCode2
//
//  Created by Bruno Del Prete on 06/03/2019.
//  Copyright © 2019 NoCode2. All rights reserved.
//

import UIKit

class BrunoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        SWIPE GESTURES
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        rightSwipe.direction = .right
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        leftSwipe.direction = .left
        
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        upSwipe.direction = .up
        
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        downSwipe.direction = .down
        
        view.addGestureRecognizer(rightSwipe)
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(upSwipe)
        view.addGestureRecognizer(downSwipe)
}
    

    @IBOutlet weak var swipeLabel: UILabel!
    
        
 
    @IBAction func handleSwipe(_ recogniser: UISwipeGestureRecognizer) {
    
//    @objc func handleSwipe(recogniser: UIGestureRecognizer) {
        if let handleSwipe = recogniser as? UISwipeGestureRecognizer {
            switch handleSwipe.direction {
            case .right:
                swipeLabel.text = "Right"
            case .left:
                swipeLabel.text = "Left"
            case .up:
                swipeLabel.text = "Up"
            case .down:
                swipeLabel.text = "Down"
            default:
                break
            }
        }
    }
    
    
    
    
    @IBAction func handlePan(_ recognizer: UIPanGestureRecognizer) {
        guard let recognizerView = recognizer.view else {
            return
        }
//        TRANSLATION
        let translation = recognizer.translation(in: view)
        recognizerView.center.x += translation.x
        recognizerView.center.y += translation.y
        recognizer.setTranslation(.zero, in: view)
        
        guard recognizer.state == .ended else {
            return
        }
        
//        Deceleration (WITHOUT BOUNDARIES)
        let velocity = recognizer.velocity(in: view)
        let vectorToFinalPoint = CGPoint(x: velocity.x / 17, y: velocity.y / 17)
        
//        let bounds = UIEdgeInsetsInsetRect(view.bounds, view.safeAreaInsets)
        let bounds = view.bounds.inset(by: view.safeAreaInsets)
        
        var finalPoint = recognizerView.center
        finalPoint.x += vectorToFinalPoint.x
        finalPoint.y += vectorToFinalPoint.y
        
        finalPoint.x = min(max(finalPoint.x, bounds.minX), bounds.maxX)
        finalPoint.y = min(max(finalPoint.y, bounds.minY), bounds.maxY)
        
        let vectorToFinalPointLength = (
            vectorToFinalPoint.x * vectorToFinalPoint.x
                + vectorToFinalPoint.y * vectorToFinalPoint.y
            ).squareRoot()
        
        UIView.animate(
            withDuration: TimeInterval(vectorToFinalPointLength / 1800),
            delay: 0,
            options: .curveEaseOut,
            animations: { recognizerView.center = finalPoint }
        )
    }
    
//    PINCH & ROTATE
    @IBAction func handlePinch(_ recognizer: UIPinchGestureRecognizer) {
        guard let recognizerView = recognizer.view else {
            return
        }
        recognizerView.transform = recognizerView.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
        recognizer.scale = 1
    }
    @IBAction func handleRotate(_ recognizer: UIRotationGestureRecognizer) {
        guard let recognizerView = recognizer.view else {
            return
        }
        recognizerView.transform = recognizerView.transform.rotated(by: recognizer.rotation)
        recognizer.rotation = 0
    }
    
    @IBAction func handleLongPress(_ recognizer: UILongPressGestureRecognizer) {
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let pressAlert = UIAlertController(title: "Long Press", message: "Brav", preferredStyle: .alert)
        let close = UIAlertAction(title: "Close", style: .default, handler: nil)
        pressAlert.addAction(close)
        self.present(pressAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func handleTap(_ sender: UITapGestureRecognizer) {
    }
    
    
}
//      AT THE SAME TIME

extension BrunoViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith _: UIGestureRecognizer) -> Bool {
        return true
    }
}


